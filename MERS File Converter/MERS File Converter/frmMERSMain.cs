﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using Microsoft.VisualBasic.FileIO;
using System.Diagnostics;

namespace MERS_File_Converter {
    public partial class MERSMainForm : Form {
        private FileStream originFile;      
        private ArrayList auditSet = new ArrayList();
        private string fileLocation;
        readonly string m01 = @"\1008662_01_" + DateTime.Now.AddDays(-1).ToString("MMddyyyy") + "_1008662.txt";
        readonly string m02 = @"\1008662_02_" + DateTime.Now.AddDays(-1).ToString("MMddyyyy") + "_1008662.txt";
        readonly string m03 = @"\1008662_03_" + DateTime.Now.AddDays(-1).ToString("MMddyyyy") + "_1008662.txt";

        public MERSMainForm() {
            InitializeComponent();
        }

        private void ParseFile() {
            try {
                using (StreamReader sr = new StreamReader(originFile)) {
                    using (TextFieldParser tfp = new TextFieldParser(sr)) {
                        tfp.SetDelimiters(",");
                        tfp.HasFieldsEnclosedInQuotes = true;
                        while (!tfp.EndOfData) {
                            string[] tempLine = tfp.ReadFields();
                            auditSet.Add(new MIN(tempLine[1], tempLine[2], tempLine[3], tempLine[4], tempLine[5], tempLine[6], tempLine[7], tempLine[8], tempLine[15],
                               tempLine[17], tempLine[18], tempLine[19], tempLine[23], tempLine[41], tempLine[42], tempLine[43], tempLine[56], tempLine[57]));
                        }
                    }
                }
            }
            catch (Exception ex) {
                string error = "Exception encountered reading file and creating objects.\n\r" + ex.StackTrace;
                MessageBox.Show(error);
                Application.Exit();
            }


            //01
            try {
                using (FileStream fs = new FileStream((fileLocation + m01), FileMode.Create, FileAccess.Write)) {
                    using (StreamWriter sw = new StreamWriter(fs)) {
                        sw.WriteLine("111111111111111111\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662");
                        int sentinel = 0;
                        foreach (MIN m in auditSet) {
                            sw.WriteLine(m.MERS01());
                            sentinel++;
                        }
                        sw.WriteLine("999999999999999999\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662\t" + sentinel);
                    }
                }
            }
            catch (Exception ex) {
                string error = "Exception encountered while writing file 01.\n\r" + ex.StackTrace;
                MessageBox.Show(error);
                Application.Exit();
            }

            //02
            try {
                using (FileStream fs = new FileStream((fileLocation + m02), FileMode.Create, FileAccess.Write)) {
                    using (StreamWriter sw = new StreamWriter(fs)) {
                        sw.WriteLine("111111111111111111\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662");
                        int sentinel = 0;
                        foreach (MIN m in auditSet) {
                            sw.WriteLine(m.MERS02());
                            sentinel++;
                        }
                        sw.WriteLine("999999999999999999\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662\t" + sentinel);
                    }
                }
            }
            catch (Exception ex) {
                string error = "Exception encountered while writing file 02.\n\r" + ex.StackTrace;
                MessageBox.Show(error);
                Application.Exit();
            }

            //03
            try {
                using (FileStream fs = new FileStream((fileLocation + m03), FileMode.Create, FileAccess.Write)) {
                    using (StreamWriter sw = new StreamWriter(fs)) {
                        sw.WriteLine("111111111111111111\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662");
                        int sentinel = 0;
                        foreach (MIN m in auditSet) {
                            sw.WriteLine(m.MERS03());
                            sentinel++;
                        }
                        sw.WriteLine("999999999999999999\t1008662\t" + DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy") + "\t1008662\t" + sentinel);
                    }
                }
            }
            catch (Exception ex) {
                string error = "Exception encountered while writing file 03.\n\r" + ex.StackTrace;
                MessageBox.Show(error);
                Application.Exit();
            }
            

            MessageBox.Show(null, "Reports have been generated to: " + fileLocation, "Reports Generated", MessageBoxButtons.OK);
            Process.Start(fileLocation);
            Application.Exit();
        }

        
        private void BtnSelectFile_Click(object sender, EventArgs e) {
            using (OpenFileDialog ofd = new OpenFileDialog()) {
                ofd.Title = "Please Select CSV File to Convert";
                ofd.Filter = "CSV File (*.csv)|*.csv";
                ofd.AddExtension = true;
                ofd.CheckFileExists = true;
                ofd.CheckPathExists = true;

                if (ofd.ShowDialog() == DialogResult.OK) {
                    this.originFile = new FileStream(ofd.FileName, FileMode.Open);
                    btnSelectFile.Text = "Select New File";
                    btnConvertFile.Enabled = true;
                    btnConvertFile.BackColor = Color.LightGreen;
                }
            }
        }

        private void BtnConvertFile_Click(object sender, EventArgs e) {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog()) {
                fbd.Description = "Please select save file location";

                if (fbd.ShowDialog() == DialogResult.OK) {
                    this.fileLocation = fbd.SelectedPath;
                }
            }
            ParseFile();
        }
    }
}
