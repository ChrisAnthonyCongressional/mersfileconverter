﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Note: Class is limited to QA Audit 2017-11 - CA
/// </summary>

namespace MERS_File_Converter {
    class MIN {
        //Constant field widths
        private const int S_MIN = 18;
        private const int S_BORROWER = 50;
        private const int S_CITY = 35;
        private const int S_STATE = 2;
        private const int S_NOTE_AMOUNT = 12;
        private const int S_NOTE_DATE = 10;
        private const int S_POOL_NUMBER = 15;
        private const int S_INVESTOR_LOAN_NUMBER = 15;
        private const int S_FHA_VA_MI_NUMBER = 20;
        private const int S_MOM_INDICATOR = 1;
        private const int S_LIEN_TYPE_DESCRIPTION = 2;
        private const int S_COUNTY_PLACE_NAME = 60;
        private const int S_SERVICER_ORG_ID = 7;
        private const int S_INVESTOR_ORG_ID = 7;
        private const int S_STREET_NUMBER = 15;
        private const int S_STREET_NAME = 35;
        private const int S_PROPERTY_UNIT = 12;
        private const int S_ZIP = 5;
        private const int S_ORIGINATING_ORG_ID = 7;
        private const int S_ORIGINAL_NOTE_HOLDER = 60;

        //Deliminator
        private const string DELIMINATOR = "\t";

        //Filler
        private readonly string FILLER_1 = new String(' ', 1);
        private readonly string FILLER_2 = new String(' ', 2);
        private readonly string FILLER_5 = new String(' ', 5);
        private readonly string FILLER_9 = new String(' ', 9);
        private readonly string FILLER_15 = new String(' ', 15);
        private readonly string FILLER_20 = new String(' ', 20);
        private readonly string FILLER_60 = new String(' ', 60);
        private readonly string FILLER_35 = new String(' ', 35);


        //Conditional Fields
        private string subservicerOrgID = new String(' ', 7);
        private string propertyPreservationCompany1OrgId = new String(' ', 7);
        private string sercuritization = new String(' ', 100);

        //Properties
        private string min;
        private string borrowerLastName;
        private string city;
        private string state;
        private string noteAmount;
        private string noteDate;
        private string poolNumber;
        private string investorLoanNumber;
        private string fhaVAMINumber;
        private string momIndicator;
        private string lienType;
        private string countyPlaceName;
        private string servicerOrgId;
        private string investorOrgId;
        private string streetNumber;
        private string streetName;
        private string propertyUnit;
        private string propertyZip;
        private string originatingOrgId;
        private string originalNoteHolder;

        public MIN(string min, string borrower, string propertyCityState, string noteAmount, string noteDate, string poolNumber, string investorLoanNumber, string fhaVAMINumber, string momIndicator, 
            string lienTypeDescription, string countyPlaceName, string servicerOrgId, string investorOrgId, string propertyStreet, string propertyUnit, string propertyZip, string originatingOrgId,
            string originalNoteHolder) {

            string noteDateMM;
            string noteDateDD;

            this.min = RightSize(min.Replace("-", ""), S_MIN);
            this.borrowerLastName = RightSize(borrower.Split(null).Last(), S_BORROWER);
            this.city = RightSize(propertyCityState.Split(',')[0], S_CITY);
            this.state = RightSize(propertyCityState.Split(',')[1], S_STATE);
            this.noteAmount = RightSize(noteAmount.Replace("$", "").Replace(",", ""), S_NOTE_AMOUNT);
            if (noteDate.Replace(" 0:00", "").Split('/')[0].Length == 1) {
                string[] temp = noteDate.Replace(" 0:00", "").Split('/');
                noteDateMM = "0" + temp[0];
            }
            else {
                string[] temp = noteDate.Replace(" 0:00", "").Split('/');
                noteDateMM = temp[0];
            }
            if (noteDate.Replace(" 0:00", "").Split('/')[1].Length == 1) {
                string[] temp = noteDate.Replace(" 0:00", "").Split('/');
                noteDateDD = "0" + temp[1];
            }
            else {
                string[] temp = noteDate.Replace(" 0:00", "").Split('/');
                noteDateDD = temp[1];
            }
            this.noteDate = noteDateMM + "/" + noteDateDD + "/" + noteDate.Split(null)[0].Split('/')[2];
            this.poolNumber = RightSize(poolNumber, S_POOL_NUMBER);
            this.investorLoanNumber = RightSize(this.investorLoanNumber, S_INVESTOR_LOAN_NUMBER);
            this.fhaVAMINumber = RightSize(fhaVAMINumber, S_FHA_VA_MI_NUMBER);
            this.momIndicator = RightSize(momIndicator, S_MOM_INDICATOR);
            if (lienTypeDescription == "First") {
                this.lienType = "01";
            }
            else if (lienTypeDescription == "Subordinate") {
                this.lienType = "02";
            }
            else {
                this.lienType = "99";
            }
            this.countyPlaceName = RightSize(countyPlaceName, S_COUNTY_PLACE_NAME);
            this.servicerOrgId = RightSize(servicerOrgId, S_SERVICER_ORG_ID);
            this.investorOrgId = RightSize(investorOrgId, S_INVESTOR_ORG_ID);
            this.streetNumber = RightSize(propertyStreet.Split(null)[0], S_STREET_NUMBER);
            this.streetName = RightSize(propertyStreet.Substring(propertyStreet.IndexOf(" "), (propertyStreet.Length - (propertyStreet.IndexOf(" ") + 1))), S_STREET_NAME);
            this.propertyUnit = RightSize(propertyUnit, S_PROPERTY_UNIT);
            this.propertyZip = RightSize(propertyZip, S_ZIP);
            this.originatingOrgId = RightSize(originatingOrgId, S_ORIGINATING_ORG_ID);
            this.originalNoteHolder = RightSize(originalNoteHolder, S_ORIGINAL_NOTE_HOLDER);
            Console.WriteLine(this.originalNoteHolder.Length);
        }

        
        private string RightSize(string s, int i) {
            if (s == null) {
                return new String(' ', i);
            }
            else if (s.Length > i) {
                return s.Substring(0, i);
            }
            else if (s.Length == i){
                return s;
            }
            else {
                return s.PadRight(i);
            }
        }

        public string MERS01() {
            return  
                    min + DELIMINATOR +
                    lienType + DELIMINATOR +
                    noteAmount + DELIMINATOR +
                    noteDate + DELIMINATOR +
                    originatingOrgId + DELIMINATOR +
                    originalNoteHolder + DELIMINATOR +
                    servicerOrgId + DELIMINATOR +
                    subservicerOrgID + DELIMINATOR +
                    investorOrgId + DELIMINATOR +
                    propertyPreservationCompany1OrgId + DELIMINATOR +
                    streetNumber + DELIMINATOR +
                    streetName + DELIMINATOR +
                    FILLER_5 + DELIMINATOR +
                    propertyUnit + DELIMINATOR +
                    city + DELIMINATOR +
                    state + DELIMINATOR +
                    propertyZip + DELIMINATOR +
                    momIndicator + DELIMINATOR +
                    investorLoanNumber + DELIMINATOR +
                    fhaVAMINumber + DELIMINATOR +
                    poolNumber + DELIMINATOR +
                    FILLER_15 + DELIMINATOR +
                    FILLER_60 + DELIMINATOR +
                    FILLER_35 + DELIMINATOR +
                    FILLER_20 + DELIMINATOR +
                    FILLER_2 + DELIMINATOR +
                    FILLER_5 + DELIMINATOR +
                    FILLER_60 + DELIMINATOR +
                    FILLER_1 + DELIMINATOR +
                    sercuritization
            ;
        }

        public string MERS02() {
            return
                min + DELIMINATOR +
                borrowerLastName + DELIMINATOR +
                FILLER_9
            ;
        }

        public string MERS03() {
            return
                min + DELIMINATOR +
                countyPlaceName
            ;
        }
    }
}
